Example 2.3

This example illustrates the use of a subroutine, SORT, to sort N values in
a list starting at location 1000. The sorted values are also stored in the
same list and again starting at location 1000. The subroutine sorts the data
using the well-known "Bubble Sort" technique. The content of register R3 is
checked at the end of every loop to find out whether the list is sorted
or not.

--This is the book's code:

  MAIN PROGRAM

            MOVE #999, R1           ; R1 <- 999
            MOVE N, R0              ; R0 <- N
          
          CALL SUBROUTINE SORT ------------------
-------------->                                 |
|                                               |
|  SUBROUTINE SORT                         <-----
|  AGAIN:    CLEAR R3                ; R3 <- 0
|  
|  NEXT:     COMPARE (R1),(R1+1)     ; Compare current and next values
|            BRANCH_IF_LOWER Skip    ; Branch if (R1) lower than (R1+1)
|            MOVE (R1),R2            ; If NOT, swap the contents
|            MOVE (R1+1),(R1)        ;
|            MOVE R2,(R1+1)          ;
|            MOVE #1, R3             ; Mark the swap
|
|  SKIP      INCREMENT R1            ; R1 <- R1+1
|            DECREMENT R0            ; R0 <- R0-1
|            BRANCH_IF_GREATER Next  ; Go to next value
|        
|            COMPARE R3, 1           ; Was there any swap?
|            BRANCH_IF_EQUAL Again   ; Repeat process
|            
|-----------RETURN SORT
        
          
  MAIN PROGRAM
          
          CALL SUBROUTINE SORT ------------------
-------------->                                 |
|                                               |
|  SUBROUTINE SORT                         <-----
|
|  AGAIN:    MOVE #999, R1           ; R1 <- 999
|            MOVE N, R0              ; R0 <- N
|            CLEAR R3                ; R3 <- 0
|  
|  NEXT:     COMPARE (R1),(R1+1)     ; Compare current and next values
|            BRANCH_IF_LOWER Skip    ; Branch if (R1) lower than (R1+1)
|            MOVE (R1),R2            ; If NOT, swap the contents
|            MOVE (R1+1),(R1)        ;
|            MOVE R2,(R1+1)          ;
|            MOVE #1, R3             ; Mark the swap
|
|  SKIP      INCREMENT R1            ; R1 <- R1+1
|            DECREMENT R0            ; R0 <- R0-1
|            BRANCH_IF_GREATER Next  ; Go to next value
|        
|            COMPARE R3, 1           ; Was there any swap?
|            BRANCH_IF_EQUAL Again   ; Repeat process
|            
|-----------RETURN SORT
          
          
          
          
          
          
          
          
          
          
          
          
          