Chapter 2: Instruction Set Arch & Design

Exercises:

2.5 :
Write a program segment that performs the operation C <- C + A * B
using each of the instruction classes indicated in exercise 2.4 above
Assume that A, B and C are memory addresses.
